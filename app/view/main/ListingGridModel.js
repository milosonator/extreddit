/**
 * Created by Michiel on 27-1-2015.
 */
/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('ExtReddit.view.main.ListingGridModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.main.ListingGrid',

    data: {
        imgSize: 81,
        nsfwThumb: "https://www.reddit.com/static/nsfw2.png"
    }
});