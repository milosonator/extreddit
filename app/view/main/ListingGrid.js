/**
 * Created by Michiel on 25-1-2015.
 */
Ext.define("ExtReddit.view.main.ListingGrid",{
    extend: 'Ext.grid.Panel',
    requires: [
        'ExtReddit.view.main.ListingGridModel',
        'Ext.grid.plugin.BufferedRenderer'
    ],
    store: Ext.create("ExtReddit.store.Listing"),
    viewModel: {
        type: 'main.ListingGrid'
    },
    autoHeight: true,
    columns: [
        {
            header: 'Preview',
            dataIndex: 'thumbnail',
            bind: {
                width: "{imgSize}"
            },
            renderer: function (value, meta, record, row, rI, cI, store, view) {
                var url = null;
                if (value === 'nsfw') {
                    url = this.getViewModel().get("nsfwThumb");
                } else {
                    url = value;
                }
                return '<img width="70" src="' + url + '" />';
            }
        }, {
            text: 'Author',
            dataIndex: 'author'
        }, {
            xtype: 'actioncolumn',
            width: 50,
            items: [{
                icon: 'https://www.reddit.com/static/aupgray.gif',
                tooltip: 'Upvote',
                handler: function(grid, rowIndex, colIndex) {
                    console.log("upvote not implemented");
                }
            }, {
                icon: 'https://www.reddit.com/static/adowngray.gif',
                tooltip: 'Downvote',
                handler: function(grid, rowIndex, colIndex) {
                    console.log("downvote not implemented");
                }
            }]
        }, {
            xtype: 'numbercolumn',
            format: '0',
            text: 'Score',
            dataIndex: 'score',
            width: 55,
            align: 'center'
        }, {
            xtype: 'templatecolumn',
            text: 'Title',
            flex: 1,
            cellWrap: true,
            tpl: "{title} <span class='domain'>{domain}</span>"
        }, {
            text: 'Subreddit',
            dataIndex: 'subreddit'
        }
    ],
    constructor: function() {
         // update panel body on selection change
        this.callParent(arguments);
        this.getSelectionModel().on('selectionchange', function(sm, selectedRecord) {
            if (selectedRecord.length) {
                var thingContainer = Ext.ComponentQuery.query('container[itemId=thingContainer]')[0],
                    thingPanel = Ext.ComponentQuery.query('component[itemId=thingPanel]')[0],
                    tpl = thingContainer.getThingTemplate();
                thingPanel.update(tpl.apply(selectedRecord[0].data));
                thingContainer.animate({
                    duration: 1000,
                    to: {
                        width: 400
                    }
                });
            }
        });
    }
});