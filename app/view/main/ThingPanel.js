/**
 * Created by Michiel on 1-2-2015.
 */
Ext.define('ExtReddit.view.main.ThingPanel', {
    extend: 'Ext.Container',
    itemId: 'thingContainer',
    //layout: 'vbox',
    items: [{
        xtype: 'panel',
        itemId: 'thingPanel',
        bodyPadding: 7
    }],
    getThingTemplate: function() {
        var tplMarkup = [
            '{author}<br/>',
            '{title}<br/>',
            '{media}<br/>'
        ];
        var tpl = Ext.create('Ext.Template', tplMarkup);
        return tpl;
    }
});