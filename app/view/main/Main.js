/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 */
Ext.define('ExtReddit.view.main.Main', {
    extend: 'Ext.container.Container',
    requires: [
        'ExtReddit.view.main.MainController',
        'ExtReddit.view.main.MainModel',
        'ExtReddit.view.main.ListingGrid',
        'ExtReddit.view.main.ThingPanel'
    ],

    xtype: 'app-main',

    controller: 'main',
    viewModel: {
        type: 'main'
    },

    layout: {
        type: 'border'
    },

    items: [{
        xtype: 'component',
        region: 'north',
        height: 40,
        padding: 10,
        style: {
            color: '#FFFFFF',
            fontSize: '24px'
        },
        bind: {
            html: '{name} - {subname}'
        }
    },
        Ext.create('ExtReddit.view.main.ThingPanel', {
            region: 'east',
            width: 1
        }),
    {
        xtype: 'container',
        width: 250,
        region: 'west',
        items: [{
            xtype: 'panel',
            title: 'Subreddits',
            bodyPadding: 5,
            items: [
                {
                    xtype: 'component',
                    html: 'Your subreddits will appear here if you login. I hope.'
                }
            ]
        }, {
            xtype: 'form',
            title: 'Login',
            url: 'https://ssl.reddit.com/api/login/',
            //standardSubmit: true,
            baseParams: {
                api_type: 'json',
                rem: true
            },
            bodyPadding: 5,
            items: [{
                xtype: 'textfield',
                fieldLabel: 'Username',
                labelAlign: 'top',
                name: 'username',
                allowBlank: false
            }, {
                xtype: 'textfield',
                fieldLabel: 'Last Name',
                labelAlign: 'top',
                inputType: 'password',
                name: 'password',
                allowBlank: false
            }],
            buttons: [{
                text: 'Submit',
                formBind: true, //only enabled once the form is valid
                disabled: true,
                handler: function (button) {
                    Ext.util.Cookies.clear("reddit_session");
                    console.log(Ext.util.Cookies.get("reddit_session"));
                    var form = this.up('form').getForm();
                    form = button.up('form');
                    console.log("form", form);
                    Ext.Ajax.request({
                        url: 'https://ssl.reddit.com/api/login/',
                        //form: form.getEl(),
                        method: 'POST',
                        disableCaching: true,
                        withCredentials: true,
                        useDefaultXhrHeader: false,
                        success: function (result, request) {
                            console.log("succes", result);
                        },
                        failure: function (result, request) {
                            console.log("failure", result);
                        }
                    });
                }
            }]
        }]
    },
        Ext.create("ExtReddit.view.main.ListingGrid", {
            region: 'center'
        })
    ]
});
