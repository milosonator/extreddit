/**
 * The main application class. An instance of this class is created by app.js when it calls
 * Ext.application(). This is the ideal place to handle application launch and initialization
 * details.
 */
Ext.define('ExtReddit.Application', {
    extend: 'Ext.app.Application',
    
    name: 'ExtReddit',

    stores: [
        'ExtReddit.store.Listing'
    ],
    
    launch: function () {
        Ext.Ajax.cors = true;
        Ext.Ajax.setUseDefaultXhrHeader(false);
    },

    /**
     * Use this to set the modhash returned by login.
     * @param modHash
     */
    setModHash: function(modHash) {
        "use strict";
        Ext.Ajax.defaultHeaders = {
            'X-Modhash': modHash
        };
    },

    clearModHash: function() {
        Ext.Ajax.defaultHeaders = {};
    }


});
