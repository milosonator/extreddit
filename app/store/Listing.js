/**
 * Created by Michiel on 25-1-2015.
 */

Ext.define('ExtReddit.data.reader.CustomJsonReader', {
    extend : 'Ext.data.reader.Json',
    alias : 'reader.customreader',
    read: function(response, readOptions) {
        var data, result;

        if (response) {
            if (response.responseText) {
                result = this.getResponseData(response);
                if (result && result.__$isError) {
                    return new Ext.data.ResultSet({
                        total  : 0,
                        count  : 0,
                        records: [],
                        success: false,
                        message: result.msg
                    });
                } else {
                    data = this.readRecords(result, readOptions);
                }
            } else {
                response.fakeTotal = 9999;
                data = this.readRecords(response, readOptions);
            }
        }

        return data || this.nullResultSet;
    }
});

Ext.define('ExtReddit.store.Listing', {
    extend: 'Ext.data.BufferedStore',
    autoLoad: true,
    model: "ExtReddit.model.Listing",
    leadingBufferZone: 10,
    pageSize: 25,
    proxy: null, // will be set by the constructor to ovveride it.
    constructor: function(config) {
        this.initConfig(config);

        var reader = Ext.create('ExtReddit.data.reader.CustomJsonReader', {
            type: 'json',
            rootProperty: 'data.children',
            totalProperty: 'fakeTotal'
        });

        var proxy = Ext.create('Ext.data.proxy.JsonP', {
            type: 'jsonp',
            url: 'https://ssl.reddit.com/.json',
            pageParam: false, //to remove param "page"
            startParam: "after",
            limitParam: "limit",
            callbackKey: 'jsonp', // I took this from an example but I don't know why, error when changed
            reader: reader
        });

        this.setProxy(proxy);

        this.callParent(arguments);
    }
});

