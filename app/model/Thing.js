Ext.define('ExtReddit.model.Thing', {
    extend: 'Ext.data.Model',
    
    fields: [
        { name: 'id', type: 'auto' },
        { name: 'name', type: 'auto' },
        { name: 'kind', type: 'auto' },
        { name: 'data', type: 'auto' }
    ]
});
