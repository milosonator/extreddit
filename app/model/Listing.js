/**
 * Created by Michiel on 25-1-2015.
 */
Ext.define('ExtReddit.model.Listing', {
    extend: 'Ext.data.Model',

    fields: [
        { name: 'id', type: 'string', mapping: 'data.id'},
        { name: 'author', type: 'string', mapping: 'data.author' },
        { name: 'subreddit', type: 'string', mapping: 'data.subreddit' },
        { name: 'comments', type: 'int', mapping: 'data.comments' },
        { name: 'title', type: 'string', mapping: 'data.title' },
        { name: 'domain', type: 'string', mapping: 'data.domain' },
        { name: 'score', type: 'int', mapping: 'data.score'},
        { name: 'thumbnail', type: 'auto', mapping: 'data.thumbnail'},
        { name: 'url', type: 'auto', mapping: 'data.url'},
        { name: 'media', type: 'auto', mapping: 'data.media.oembed.html'}
    ],
    idProperty: 'id'
});
