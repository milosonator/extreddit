/**
 * Created by Michiel on 28-1-2015.
 */
/**
 * Overriding the normal JSON reader to inject our fakeTotal, to make the store
 * believe there are allways more records. May need to improve this on small subreddits that
 * do not have so many posts.
 */
Ext.override('Ext.data.reader.Json', {
    getResponseData: function(response) {
        console.log("hello");
        try {
            var json = Ext.decode(response.responseText);
            json.fakeTotal = 999999; // its over 9000!
            console.log("json", json);
            return json;
        } catch (ex) {
            Ext.Logger.warn('Unable to parse the JSON returned by the server');
            return this.createReadError(ex.message);
        }
    }
});